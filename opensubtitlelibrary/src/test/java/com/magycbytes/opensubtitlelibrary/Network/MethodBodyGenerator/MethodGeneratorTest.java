package com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator;

import com.magycbytes.opensubtitlelibrary.Models.Network.LoginProfile;

import org.junit.Assert;
import org.junit.Test;

import static com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.CorrectData.mCorrectExtractLanguage;
import static com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.CorrectData.mCorrectLoginMethod;
import static com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.CorrectData.mCorrectSearchSubtitleMethod;

/**
 * Created by alexandru on 6/25/16.
 */
public class MethodGeneratorTest {



    @Test
    public void languageExtractMethod() throws Exception {
        String languagesMethod = MethodGenerator.getLanguages("en");
        Assert.assertEquals(mCorrectExtractLanguage, languagesMethod);
    }

    @Test
    public void searchSubtitleMethod() throws Exception {
        String searchSubtitleMethod = MethodGenerator.searchSubtitle(new SearchSubtitleParameters("d3da", "cd, ca", "hash",
                567));
        Assert.assertEquals(mCorrectSearchSubtitleMethod, searchSubtitleMethod);
    }

    @Test
    public void loginMethod() throws Exception {
        String logInMethod = MethodGenerator.logIn(new LoginProfile("name", "pass", "en",
                "vision player"));
        Assert.assertEquals(mCorrectLoginMethod, logInMethod);
    }

}