package com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator;

/**
 * Created by alexandru on 6/25/16.
 */

public class CorrectData {
    public static String mCorrectExtractLanguage = "<methodCall>\n" +
            "   <methodName>GetSubLanguages</methodName>\n" +
            "   <params>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <string>en</string>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "   </params>\n" +
            "</methodCall>";

    public static String mCorrectLoginMethod = "<methodCall>\n" +
            "   <methodName>LogIn</methodName>\n" +
            "   <params>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <string>name</string>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <string>pass</string>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <string>en</string>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <string>vision player</string>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "   </params>\n" +
            "</methodCall>";

    public static String mCorrectSearchSubtitleMethod = "<methodCall>\n" +
            "   <methodName>SearchSubtitles</methodName>\n" +
            "   <params>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <string>d3da</string>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "      <param>\n" +
            "         <value>\n" +
            "            <array>\n" +
            "               <data>\n" +
            "                  <value>\n" +
            "                     <struct>\n" +
            "                        <member>\n" +
            "                           <name>sublanguageid</name>\n" +
            "                           <value>\n" +
            "                              <string>cd, ca</string>\n" +
            "                           </value>\n" +
            "                        </member>\n" +
            "                        <member>\n" +
            "                           <name>moviehash</name>\n" +
            "                           <value>\n" +
            "                              <string>hash</string>\n" +
            "                           </value>\n" +
            "                        </member>\n" +
            "                        <member>\n" +
            "                           <name>moviebytesize</name>\n" +
            "                           <value>\n" +
            "                              <double>567.0</double>\n" +
            "                           </value>\n" +
            "                        </member>\n" +
            "                     </struct>\n" +
            "                  </value>\n" +
            "               </data>\n" +
            "            </array>\n" +
            "         </value>\n" +
            "      </param>\n" +
            "   </params>\n" +
            "</methodCall>";
}
