package com.magycbytes.opensubtitlelibrary;

import android.util.Log;

/**
 * Created by tyranul on 3/14/16.
 */
public class SubtitlesEncodingConverter {
    private static final String CP1250 = "CP1250"; //NON-NLS
    private static final String WINDOWS_1250 = "windows-1250"; //NON-NLS
    private static final String CP1251 = "CP1251"; //NON-NLS
    private static final String WINDOWS_1251 = "windows-1251"; //NON-NLS
    private static final String CP1252 = "CP1252"; //NON-NLS
    private static final String WINDOWS_1252 = "windows-1252"; //NON-NLS
    private static final String CP1256 = "CP1256"; //NON-NLS
    private static final String WINDOWS_1256 = "windows-1256"; //NON-NLS
    private static final String UTF_8 = "UTF-8"; //NON-NLS
    private static final String US_ASCII = "US-ASCII"; //NON-NLS
    private static final String CP1254 = "CP1254"; //NON-NLS
    private static final String WINDOWS_1254 = "windows-1254"; //NON-NLS
    private static final String CP1255 = "CP1255"; //NON-NLS
    private static final String WINDOWS_1255 = "windows-1255"; //NON-NLS
    private static final String CP1253 = "CP1253"; //NON-NLS
    private static final String WINDOWS_1253 = "windows-1253"; //NON-NLS
    private static final String ASCII = "ASCII";

    public static String convert(String encodingToTransform) {
        Log.i("EncodingConverter", "Encoding to transform: " + encodingToTransform);
        switch (encodingToTransform) {
            case CP1250:
                return WINDOWS_1250;
            case CP1251:
                return WINDOWS_1251;
            case CP1252:
                return WINDOWS_1252;
            case CP1253:
                return WINDOWS_1253;
            case CP1254:
                return WINDOWS_1254;
            case CP1255:
                return WINDOWS_1255;
            case CP1256:
                return WINDOWS_1256;
            case UTF_8:
                return UTF_8;
            case ASCII:
                return US_ASCII;
            default:
                return UTF_8;
        }
    }
}
