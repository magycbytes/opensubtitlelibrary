package com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator;

/**
 * Created by alexandru on 6/26/16.
 */

public class SearchSubtitleParameters {
    private String mAccessToken;
    private String mSubtitleLanguage;
    private String mMovieHash;
    private double mMovieSize;

    public SearchSubtitleParameters() {
    }

    public SearchSubtitleParameters(String accessToken, String subtitleLanguage, String movieHash, double movieSize) {
        mAccessToken = accessToken;
        mSubtitleLanguage = subtitleLanguage;
        mMovieHash = movieHash;
        mMovieSize = movieSize;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getSubtitleLanguage() {
        return mSubtitleLanguage;
    }

    public void setSubtitleLanguage(String subtitleLanguage) {
        mSubtitleLanguage = subtitleLanguage;
    }

    public String getMovieHash() {
        return mMovieHash;
    }

    public void setMovieHash(String movieHash) {
        mMovieHash = movieHash;
    }

    public double getMovieSize() {
        return mMovieSize;
    }

    public void setMovieSize(double movieSize) {
        mMovieSize = movieSize;
    }
}
