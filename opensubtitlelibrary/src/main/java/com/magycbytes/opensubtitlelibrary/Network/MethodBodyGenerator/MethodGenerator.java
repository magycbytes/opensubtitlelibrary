package com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator;

import com.magycbytes.opensubtitlelibrary.Models.Network.Array;
import com.magycbytes.opensubtitlelibrary.Models.Network.Data;
import com.magycbytes.opensubtitlelibrary.Models.Network.Member;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodCall;
import com.magycbytes.opensubtitlelibrary.Models.Network.Param;
import com.magycbytes.opensubtitlelibrary.Models.Network.Params;
import com.magycbytes.opensubtitlelibrary.Models.Network.Struct;
import com.magycbytes.opensubtitlelibrary.Models.Network.Value;
import com.magycbytes.opensubtitlelibrary.Models.Network.LoginProfile;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandru on 6/25/16.
 */

public class MethodGenerator {

    public static String getLanguages(String interfaceLanguage) throws Exception {

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param(new Value(interfaceLanguage)));

        MethodCall methodCall = new MethodCall("GetSubLanguages", new Params(paramList));

        return serialize(methodCall);
    }

    public static String downloadSubtitle(String accessToken, List<Integer> mSubtitlesIds) throws Exception {

        List<Value> values = new ArrayList<>(mSubtitlesIds.size());
        for (Integer integer : mSubtitlesIds) {
            values.add(new Value(integer));
        }

        Param accessTokenParam = new Param(new Value(accessToken));
        Param idsParam = new Param(new Value(new Array(new Data(values))));

        List<Param> paramList = new ArrayList<>(2);
        paramList.add(accessTokenParam);
        paramList.add(idsParam);

        MethodCall methodCall = new MethodCall("DownloadSubtitles", new Params(paramList));

        return serialize(methodCall);
    }

    public static String searchSubtitle(SearchSubtitleParameters parameters) throws Exception {

        Param accessTokenParam = new Param(new Value(parameters.getAccessToken()));

        List<Member> members = new ArrayList<>(3);
        members.add(new Member("sublanguageid", new Value(parameters.getSubtitleLanguage())));
        members.add(new Member("moviehash", new Value(parameters.getMovieHash())));
        members.add(new Member("moviebytesize", new Value(parameters.getMovieSize())));

        Value value = new Value(new Struct(members));
        List<Value> values = new ArrayList<>(1);
        values.add(value);

        Param dataParam = new Param(new Value(new Array(new Data(values))));

        List<Param> paramList = new ArrayList<>(2);
        paramList.add(accessTokenParam);
        paramList.add(dataParam);

        MethodCall methodCall = new MethodCall("SearchSubtitles", new Params(paramList));
        return serialize(methodCall);
    }

    public static String logIn(LoginProfile loginProfile) throws Exception {

        List<Param> paramList = new ArrayList<>(4);
        paramList.add(new Param(new Value(loginProfile.getName())));
        paramList.add(new Param(new Value(loginProfile.getPassword())));
        paramList.add(new Param(new Value(loginProfile.getUiLanguage())));
        paramList.add(new Param(new Value(loginProfile.getClientName())));

        MethodCall methodCall = new MethodCall("LogIn", new Params(paramList));
        return serialize(methodCall);
    }

    private static String serialize(MethodCall methodCall) throws Exception {

        Serializer serializer = new Persister(new AnnotationStrategy());
        StringWriter stringWriter = new StringWriter();

        serializer.write(methodCall, stringWriter);

        return stringWriter.getBuffer().toString();
    }

}
