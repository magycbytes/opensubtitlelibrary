package com.magycbytes.opensubtitlelibrary.Models.UserResult;

/**
 * Created by alexandru on 6/25/16.
 */

public class SubtitleLanguage {

    private String mLanguageName;
    private String mIso639;

    public SubtitleLanguage() {
    }

    public SubtitleLanguage(String languageName, String iso639) {
        mLanguageName = languageName;
        mIso639 = iso639;
    }

    public String getLanguageName() {
        return mLanguageName;
    }

    public void setLanguageName(String languageName) {
        mLanguageName = languageName;
    }

    public String getIso639() {
        return mIso639;
    }

    public void setIso639(String iso639) {
        mIso639 = iso639;
    }

    @Override
    public String toString() {
        return String.format("%s, %s", mLanguageName, mIso639);
    }
}
