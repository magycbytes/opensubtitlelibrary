package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by alexandru on 6/25/16.
 */

public class Params {

    @ElementList(inline = true)
    private List<Param> mParams;

    public Params() {

    }

    public Params(List<Param> params) {
        mParams = params;
    }

    public List<Param> getParams() {
        return mParams;
    }

    public void setParams(List<Param> params) {
        mParams = params;
    }
}
