package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by alexandru on 6/25/16.
 */

public class Struct {

    @ElementList(inline = true)
    private List<Member> mMembers;

    public Struct() {

    }

    public Struct(List<Member> members) {
        mMembers = members;
    }

    public List<Member> getMembers() {
        return mMembers;
    }

    public void setMembers(List<Member> members) {
        mMembers = members;
    }
}
