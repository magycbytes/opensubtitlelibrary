package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by alexandru on 6/25/16.
 */

@Root(name = "methodResponse")
public class MethodResponse {

    @Element(name = "params")
    private Params mParams;

    public Params getParams() {
        return mParams;
    }

    public void setParams(Params params) {
        mParams = params;
    }
}
