package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.Element;

/**
 * Created by alexandru on 6/25/16.
 */

public class Array {

    @Element(name = "data")
    private Data mData;

    public Array() {

    }

    public Array(Data data) {
        mData = data;
    }

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }
}
