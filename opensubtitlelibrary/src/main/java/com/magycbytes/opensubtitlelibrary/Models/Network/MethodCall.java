package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by acebotari on 4/25/16.
 */
@Root(name = "methodCall")
public class MethodCall {

    @Element(name = "methodName")
    private String mMethodName;

    @Element(name = "params")
    private Params mParams;

    public MethodCall() {

    }

    public MethodCall(String methodName, Params params) {
        mMethodName = methodName;
        mParams = params;
    }

    public String getMethodName() {
        return mMethodName;
    }

    public void setMethodName(String methodName) {
        mMethodName = methodName;
    }

    public Params getParams() {
        return mParams;
    }

    public void setParams(Params params) {
        mParams = params;
    }
}
