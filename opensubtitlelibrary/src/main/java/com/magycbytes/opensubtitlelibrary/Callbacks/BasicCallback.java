package com.magycbytes.opensubtitlelibrary.Callbacks;

import com.magycbytes.opensubtitlelibrary.Models.Network.Member;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;
import com.magycbytes.opensubtitlelibrary.Models.Network.Value;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandru on 6/26/16.
 */

public abstract class BasicCallback implements Callback<MethodResponse> {

    List<Value> getValues(Response<MethodResponse> response) {
        List<Member> members = getMembers(response);
        for (int i = 0; i < members.size(); ++i) {
            if (members.get(i).getName().equals("data")) {
                return members.get(i).getValue().getArray().getData().getValues();
            }
        }
        return null;
    }

    List<Member> getMembers(Response<MethodResponse> response) {
        return response.body().getParams().getParams().get(0).getValue().getStruct
                ().getMembers();
    }
}
