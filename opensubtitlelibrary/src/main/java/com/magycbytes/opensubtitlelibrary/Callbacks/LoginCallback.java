package com.magycbytes.opensubtitlelibrary.Callbacks;

import android.util.Log;

import com.magycbytes.opensubtitlelibrary.Models.Network.Member;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by alexandru on 6/26/16.
 */

public class LoginCallback extends BasicCallback {

    private OnLogin mOnLoginListener;

    public LoginCallback(OnLogin onLoginListener) {
        if (onLoginListener == null) {
            throw new IllegalArgumentException("onLoginListener");
        }
        mOnLoginListener = onLoginListener;
    }

    @Override
    public void onResponse(Call<MethodResponse> call, Response<MethodResponse> response) {
        List<Member> members = getMembers(response);

        for (int i = 0; i < members.size(); ++i) {
            if (members.get(i).getName().equals("token")) {
                String accessToken = members.get(i).getValue().getString();
                mOnLoginListener.loginWithSuccess(accessToken);
                return;
            }
        }
        Log.e(this.toString(), "No token found on response");
        mOnLoginListener.loginError();
    }

    @Override
    public void onFailure(Call<MethodResponse> call, Throwable t) {
        Log.e(this.toString(), t.getMessage());
        mOnLoginListener.loginError();
    }

    public interface OnLogin {
        void loginWithSuccess(String accessToken);

        void loginError();
    }
}
