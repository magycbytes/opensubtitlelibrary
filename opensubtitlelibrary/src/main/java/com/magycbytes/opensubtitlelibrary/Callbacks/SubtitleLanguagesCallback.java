package com.magycbytes.opensubtitlelibrary.Callbacks;

import android.util.Log;

import com.magycbytes.opensubtitlelibrary.Models.Network.Member;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;
import com.magycbytes.opensubtitlelibrary.Models.Network.Value;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.SubtitleLanguage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by alexandru on 6/25/16.
 */

public class SubtitleLanguagesCallback extends BasicCallback {

    private OnLanguagesExtract mLanguageExtractListener;

    public SubtitleLanguagesCallback(OnLanguagesExtract languageExtractListener) {
        if (languageExtractListener == null) {
            throw new IllegalArgumentException("language extractor listener can't be null");
        }
        mLanguageExtractListener = languageExtractListener;
    }

    @Override
    public void onResponse(Call<MethodResponse> call, Response<MethodResponse> response) {
        List<Value> values = getValues(response);
        if (values == null) {
            Log.e(this.toString(), "Not found values in response");
            mLanguageExtractListener.errorExtractingLanguages();
            return;
        }

        List<SubtitleLanguage> subtitleLanguages = new ArrayList<>();
        for (int i = 0; i < values.size(); ++i) {
            subtitleLanguages.add(parseOneItem(values.get(i).getStruct().getMembers()));
        }
        mLanguageExtractListener.successExtractingLanguages(subtitleLanguages);
    }

    @Override
    public void onFailure(Call<MethodResponse> call, Throwable t) {
        mLanguageExtractListener.errorExtractingLanguages();
    }

    private SubtitleLanguage parseOneItem(List<Member> members) {
        SubtitleLanguage subtitleLanguage = new SubtitleLanguage();

        for (int j = 0; j < members.size(); ++j) {
            String valueString = members.get(j).getValue().getString();
            switch (members.get(j).getName()) {
                case "LanguageName":
                    subtitleLanguage.setLanguageName(valueString);
                    break;
                case "ISO639":
                    subtitleLanguage.setIso639(valueString);
                    break;
            }
        }
        return subtitleLanguage;
    }

    public interface OnLanguagesExtract {
        void successExtractingLanguages(List<SubtitleLanguage> subtitleLanguages);

        void errorExtractingLanguages();
    }
}
