package com.magycbytes.opensubtitlelibrary.Callbacks;

import android.util.Log;

import com.magycbytes.opensubtitlelibrary.Models.Network.Member;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;
import com.magycbytes.opensubtitlelibrary.Models.Network.Value;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.DownloadedSubtitle;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by alexandru on 6/25/16.
 */

public class DownloadSubtitleCallback extends BasicCallback {

    private OnDownload mOnDownloadListener;

    public DownloadSubtitleCallback(OnDownload onDownloadListener) {
        mOnDownloadListener = onDownloadListener;
    }

    @Override
    public void onResponse(Call<MethodResponse> call, Response<MethodResponse> response) {
        List<Value> values = getValues(response);
        if (values == null || values.size() == 0) {
            Log.e(this.toString(), "Not found values in response");
            mOnDownloadListener.errorDownloading();
            return;
        }

        List<Member> members = values.get(0).getStruct().getMembers();
        DownloadedSubtitle downloadedSubtitle = parseMembers(members);
        mOnDownloadListener.downloadedWithSuccess(downloadedSubtitle);
    }

    private DownloadedSubtitle parseMembers(List<Member> members) {
        for (int i = 0; i < members.size(); ++i) {
            if (members.get(i).getName().equals("data")) {
                return new DownloadedSubtitle(members.get(i).getValue().getString());
            }
        }
        return null;
    }

    @Override
    public void onFailure(Call<MethodResponse> call, Throwable t) {
        Log.e(this.toString(), t.getMessage());
        mOnDownloadListener.errorDownloading();
    }

    public interface OnDownload {
        void downloadedWithSuccess(DownloadedSubtitle downloadedSubtitle);

        void errorDownloading();
    }
}
