package com.magycbytes.testingopensubtitlelibrary;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.magycbytes.opensubtitlelibrary.Callbacks.DownloadSubtitleCallback;
import com.magycbytes.opensubtitlelibrary.Callbacks.LoginCallback;
import com.magycbytes.opensubtitlelibrary.Callbacks.SearchSubtitleCallback;
import com.magycbytes.opensubtitlelibrary.Callbacks.SubtitleLanguagesCallback;
import com.magycbytes.opensubtitlelibrary.Models.Network.LoginProfile;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.DownloadedSubtitle;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.SubtitleLanguage;
import com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.MethodGenerator;
import com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.SearchSubtitleParameters;
import com.magycbytes.opensubtitlelibrary.Network.NetworkApiManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity
        implements LoginCallback.OnLogin, SubtitleLanguagesCallback.OnLanguagesExtract, SearchSubtitleCallback.OnSearchSubtitle, DownloadSubtitleCallback.OnDownload {

    private String mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        extractLanguages();
        logIn();
    }

    private void logIn() {
        LoginProfile loginProfile = new LoginProfile("", "", Locale.getDefault().getLanguage(),
                "visionplayer");

        RequestBody requestBody;
        try {
            requestBody = RequestBody.create(MediaType.parse("text/plain"),
                    MethodGenerator.logIn(loginProfile));
        } catch (Exception e) {
            Log.e(this.toString(), e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<MethodResponse> call = NetworkApiManager.getApi().sendRequest(requestBody);
        call.enqueue(new LoginCallback(this));
    }

    private void extractLanguages() {
        RequestBody requestBody;
        try {
            requestBody = RequestBody.create(MediaType.parse("text/plain"),
                    MethodGenerator.getLanguages(Locale.getDefault().getLanguage()));
        } catch (Exception e) {
            Log.e(this.toString(), e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }
        Call<MethodResponse> call = NetworkApiManager.getApi().sendRequest(requestBody);
        call.enqueue(new SubtitleLanguagesCallback(this));
    }

    private void searchSubtitles() {

        SearchSubtitleParameters parameters = new SearchSubtitleParameters(
                mAccessToken,
                "cze,eng,ger,slo",
                "7d9cd5def91c9432",
                735934464
        );

        RequestBody requestBody;
        try {
            String content = MethodGenerator.searchSubtitle(parameters);
            Log.i(this.toString(), content);
            requestBody = RequestBody.create(MediaType.parse("text/plain"),
                    content);
        } catch (Exception e) {
            Log.e(this.toString(), e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }
        Call<MethodResponse> call = NetworkApiManager.getApi().sendRequest(requestBody);
        call.enqueue(new SearchSubtitleCallback(this));
    }

    void downloadSubtitle() {
        RequestBody requestBody;
        List<Integer> mSubtitleIds = new ArrayList<>();
        mSubtitleIds.add(1951894257);

        try {
            String content = MethodGenerator.downloadSubtitle(mAccessToken, mSubtitleIds);
            Log.i(this.toString(), content);
            requestBody = RequestBody.create(MediaType.parse("text/plain"), content);
        } catch (Exception e) {
            Log.e(this.toString(), e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }
        Call<MethodResponse> call = NetworkApiManager.getApi().sendRequest(requestBody);
        call.enqueue(new DownloadSubtitleCallback(this));
    }

    @Override
    public void loginWithSuccess(String accessToken) {
        mAccessToken = accessToken;
        Toast.makeText(this, "Login with success: " + accessToken, Toast.LENGTH_SHORT).show();
//        searchSubtitles();
        downloadSubtitle();
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Error at login", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successExtractingLanguages(List<SubtitleLanguage> subtitleLanguages) {
        Toast.makeText(this, "Success extracting: " + subtitleLanguages.size(), Toast
                .LENGTH_SHORT).show();
    }

    @Override
    public void errorExtractingLanguages() {
        Toast.makeText(this, "Error at extracting languages", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void foundSubtitles(List<Subtitle> subtitles) {
        Toast.makeText(this, "Success searching subtitles: " + subtitles.size(), Toast
                .LENGTH_SHORT).show();
        Log.i(this.toString(), "Found :" + subtitles.size());
    }

    @Override
    public void errorSearchingSubtitle() {
        Toast.makeText(this, "Error at searching subtitles", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void downloadedWithSuccess(DownloadedSubtitle downloadedSubtitle) {
        Toast.makeText(this, "Success downloading subtitle: " + downloadedSubtitle
                .getSubtitleContent().length(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void errorDownloading() {
        Toast.makeText(this, "Error at downloading subtitles", Toast.LENGTH_SHORT).show();
    }
}
